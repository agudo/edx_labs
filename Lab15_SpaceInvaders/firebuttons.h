// Piano.h
// Runs on LM4F120 or TM4C123, 
// edX lab 13 
// There are four keys in the piano
// Daniel Valvano, Jonathan Valvano
// March 13, 2014

#define NONE     0x0  
#define FIRE1    0x01 
#define FIRE2    0x02 
#define FIRE12   0x04 


// **************Piano_Init*********************
// Initialize fire key inputs
// Input: none
// Output: none
void Fire_Init(void); 
  

// **************Piano_In*********************
// Input from piano key inputs
// Input: none 
// Output: 0 to 15 depending on keys
// 0x01 is key 0 pressed, 0x02 is key 1 pressed,
unsigned long Fire_In(void);

void FireOff_LEDS 		 (void);
void Fire_LED  				 (void);
void Special_FireLED   (void);

// **************Piano_Testing *********************
// output  Test Piano
int Fire_Testing (void);
