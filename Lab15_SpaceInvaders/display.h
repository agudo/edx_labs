#ifndef __MYDISPLAY_H__
#define __MYDISPLAY_H__

// Display.h
// Jagudo
// 27 Apr 2014
// In chargue of displaying the graphics on the screen.

void Screen_Init        (void);
void Screen_Entry       (void); 
void Screen_Menu        (void); 
void Screen_ShowDead    (void);
void Screen_ShowLives   (unsigned long livesleft); 
void Screen_Refresh     (unsigned long count); // time to delay if different of 0
void Screen_Test	      (unsigned long count); // time delay in 0.1 seconds
void Screen_GameOver    (void);

#endif // __MYDISPLAY_H__

