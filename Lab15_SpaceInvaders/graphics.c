#include "graphics.h"
#include "images.h"
#include "Nokia5110.h"

// DEFINES
// -------

#define BLINK_DELAY        7
#define NB_OF_ENEMIES      12
#define NB_OF_ENEMIES_ROW  4
#define NB_OF_AMUNITION   10
#define KILLME          9999


// EXTERNALS
// ---------
extern void Delay100ms(long count); // time delay in 0.1 seconds
extern unsigned long Refresh;

// STRUCTURES
// ----------
struct State
{
	unsigned long x; // x coordinate
	unsigned long y; // y coordinate
	unsigned long delay; // delay time 
	const unsigned char *image; //ptr->image
	long life; // 0 = dead, 1 = alive
};
typedef struct State SType;
typedef enum {BUNKER3, BUNKER2, BUNKER1, BUNKER0} bunkers_enum;

// LOCAL GLOBALS
// -------------
SType Enemy 		      [NB_OF_ENEMIES];
SType EnemyFire       [NB_OF_ENEMIES];
SType BattleShip      [1];
SType BattleShipFire  [NB_OF_AMUNITION];
SType Bunkers         [3];

int currentFire;


// HELPERS
// -------
void DrawEnemies        (void);
void DrawEnemyFire      (void);
void DrawBunkers        (void);
void DrawBattleShip     (unsigned long lives);
void DrawBattleShipFire (void);
void Flush              (SType *type, int count);
unsigned long  abs      (int x);
unsigned long  max      (int x, int y);
unsigned long  min      (int x, int y);

void SpriteInit (unsigned long lives)
{
	DrawEnemies 	 ();
	DrawBattleShip (lives);
	DrawBunkers    ();
}

// Move Sprites into screen Buffer.
void SpritesDraw     (void)
{
	Nokia5110_Clear();
	Nokia5110_ClearBuffer();
  Flush (Enemy, NB_OF_ENEMIES);	
	Flush (EnemyFire, NB_OF_ENEMIES);	
	Flush (Bunkers, 3);	
	Flush (BattleShip, 1);	
	Flush (BattleShipFire, NB_OF_AMUNITION);	
}

// Move Ship and / or update fires.
void SpritesMoveShip (unsigned long x)
{
	int idx;

	BattleShip[0].x = min (83 - PLAYERW, max ( 0,  x));
  
	for (idx = 0; idx < NB_OF_AMUNITION ; ++idx)
	{
		if (BattleShipFire[idx].delay == 0)
		{
			BattleShipFire[idx].y    = max (0, BattleShipFire[idx].y - LASERH);
			BattleShipFire[idx].life = BattleShipFire[idx].y > 0 ?  1 :  0;
		}
		else 
			BattleShipFire[idx].delay -= 1;
	}
	
	
	
}

void SpriteDrawFire    (void)
{
	BattleShipFire [currentFire].x = BattleShip[0].x + (PLAYERW >> 1);
	BattleShipFire [currentFire].y = BattleShip[0].y - PLAYERH ;
	BattleShipFire [currentFire].life = 1;
	BattleShipFire [currentFire].delay = BLINK_DELAY;
	BattleShipFire [currentFire].image = Laser0;
	
	currentFire = (currentFire + 1) % 10;
}

void SpriteDrawMissile (void)
{
	BattleShipFire [currentFire].x = BattleShip[0].x + (PLAYERW >> 1);
	BattleShipFire [currentFire].y = BattleShip[0].y - PLAYERH ;
	BattleShipFire [currentFire].life = 1;
	BattleShipFire [currentFire].delay = BLINK_DELAY;
	BattleShipFire [currentFire].image = Missile0;
	
	currentFire = (currentFire + 1) % 10;
}


// Move All enemies sprites and returns weather or not our ship has been touched.
unsigned short SpritesEnemyMoves	(void)
{
	int i, j; 
	unsigned short battleship_touched = 0;

	for (i = 0; i < NB_OF_ENEMIES ; ++i)
	{
		
		if (Enemy[i].life == 0)
			continue;


		// Check collisions on enemies
		for (j = 0; j < currentFire; ++j)
		{
			if (abs (abs (Enemy[i].x - BattleShipFire [j].x) -
							 abs (Enemy[i].y - BattleShipFire [j].y)) < 3)
			{
					Enemy[i].life = KILLME;
					Enemy[i].image = SmallExplosion0 ;
			}
		}

			if (Enemy[i].delay == 0)
			{	  
				  if (Enemy[i].life == KILLME)
					{
						Enemy[i].life = 0;
					}
					else
					{
						Enemy[i].delay = BLINK_DELAY;
						if (Enemy[i].image == SmallEnemy30PointB) Enemy[i].image = SmallEnemy30PointA;
						else                                      Enemy[i].image = SmallEnemy30PointB;
					}
			}
			Enemy[i].delay -= 1;
	}

  return battleship_touched;
}

void SpritesTesting (void)
{

	Nokia5110_PrintBMP(32, 47,  				 PlayerShip0, 0); // player ship middle bottom
  Nokia5110_PrintBMP(33, 47 - PLAYERH, Bunker0, 		0);

  Nokia5110_PrintBMP(0,  ENEMY10H - 1, SmallEnemy10PointA, 0);
  Nokia5110_PrintBMP(16, ENEMY10H - 1, SmallEnemy20PointA, 0);
  Nokia5110_PrintBMP(32, ENEMY10H - 1, SmallEnemy20PointA, 0);
  Nokia5110_PrintBMP(48, ENEMY10H - 1, SmallEnemy30PointA, 0);
  Nokia5110_PrintBMP(64, ENEMY10H - 1, SmallEnemy30PointA, 0);

	Refresh = 1;
}

void SpriteCollisions (void)
{
	
}

void SpriteEntryScreen (void)
{
		// Nokia5110_PrintBMP(0, MARQUEEH - 1, Marquee, 4);
}


// HELPERS IMPLEMENTATION
// ----------------------

void DrawEnemies (void)
{
		int i, j, idx = 0;
	// Enemies
	for (i = 0; i < (NB_OF_ENEMIES / NB_OF_ENEMIES_ROW) ; ++i)
	{
		for (j = 0; j <  NB_OF_ENEMIES_ROW  ; ++j)
		{
			Enemy[idx].x = 20 * j;
			Enemy[idx].y = 10 * i + 10;
			Enemy[idx].delay = 5;
			Enemy[idx].image = SmallEnemy30PointB;
			Enemy[idx].life = 1;
			++idx;
		}
	}
}

void DrawEnemyFire (void)
{
	int i; 
	for (i = 0; i < NB_OF_ENEMIES ; ++i)
	{
		EnemyFire[i].x = 20 * i;
		EnemyFire[i].y = 10;
		EnemyFire[i].image = Missile0;
		EnemyFire[i].life = 0;
	}
	
}

void DrawBunkers (void)
{
  int i;
	// Bunkers
	for (i = 0; i < 3 ; ++i)
	{
		Bunkers[i].x = 33 * i;
		Bunkers[i].y = 47 - PLAYERH;
		Bunkers[i].image = Bunker0;
		Bunkers[i].life = BUNKER0;
	}
	
}


void DrawBattleShip (unsigned long lives)
{
	// Battleship
	BattleShip[0].x = 32;
	BattleShip[0].y = 47;
	BattleShip[0].image = PlayerShip0;
	BattleShip[0].life  = lives;
}

void DrawBattleShipFire (void)
{
	int i;
	
	currentFire = 0;
	
	for (i = 0; i < NB_OF_AMUNITION ; ++i)
	{
			BattleShipFire[i].x = 32;
			BattleShipFire[i].y = 47;
			BattleShipFire[i].image = Laser0;
			BattleShipFire[i].life  = 0;
	}
}

void Flush (SType *type, int count)
{
	int i;
	for (i = 0; i < count; ++i)
	{
		if (type[i].life > 0)
		{
			Nokia5110_PrintBMP (type[i].x, type[i].y, type[i].image, 0);
		}
	}
}

unsigned long  abs (int x)
{
	return x > 0 ? x : -1 * x;
}

unsigned long  max      (int x, int y)
{
	return x > y ? x : y;
}

unsigned long  min      (int x, int y)
{
	return x > y ? y : x;
}
