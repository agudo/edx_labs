// ***** 0. Documentation Section *****
// TableTrafficLight.c for Lab 10
// Runs on LM4F120/TM4C123
// Index implementation of a Moore finite state machine to operate a traffic light.  
// Daniel Valvano, Jonathan Valvano
// November 7, 2013

// east/west red light connected to PB5
// east/west yellow light connected to PB4
// east/west green light connected to PB3
// north/south facing red light connected to PB2
// north/south facing yellow light connected to PB1
// north/south facing green light connected to PB0
// pedestrian detector connected to PE2 (1=pedestrian present)
// north/south car detector connected to PE1 (1=car present)
// east/west car detector connected to PE0 (1=car present)
// "walk" light connected to PF3 (built-in green LED)
// "don't walk" light connected to PF1 (built-in red LED)

// ***** 1. Pre-processor Directives Section *****
#include "TExaS.h"
#include "tm4c123gh6pm.h"

// ***** 2. Global Structures Section *****

struct State 
{
	unsigned long OutCarLights;
	unsigned long OutPeatonLights;
	unsigned long Time;
	const struct State *Next[8];
};

typedef const struct State SType;

// ***** 3. Globals Sections *****
#define init2South  	   &SFSM [0]
#define goSouth    			 &SFSM [1]
#define steadyGreenSouth &SFSM [2]
#define yellowSouth      &SFSM [3]
#define waitSouth        &SFSM [4]
#define goWest           &SFSM [5]
#define steadyGreenWest  &SFSM [6]
#define yellowWest       &SFSM [7]
#define waitWest         &SFSM [8]
#define walk             &SFSM [9]
#define steadyGreenWalk  &SFSM [10]
#define hurryupOff1      &SFSM [11]
#define hurryupOn1       &SFSM [12]
#define hurryupOff2      &SFSM [13]
#define hurryupOn2       &SFSM [14]
#define hurryupOff3      &SFSM [15]
#define hurryupOn3       &SFSM [16]
#define dontWalk         &SFSM [17]
#define init2West        &SFSM [18]

#define patt1        		 &SFSM [0]
#define patt2        		 &SFSM [1]
#define patt3        		 &SFSM [2]
#define patt4        		 &SFSM [3]
#define patt5        		 &SFSM [4]
#define patt6        		 &SFSM [5]
#define patt7        		 &SFSM [6]
#define patt8        		 &SFSM [7]
#define patt9        		 &SFSM [8]

// Choosing port PE for Lights ...
#define CARLIGHTS       (* ((volatile unsigned long *) 0x400053FC))  // PB
#define WALKLIGHTS      (* ((volatile unsigned long *) 0x400253FC))  // PF3, PF1
#define SENSORS         (* ((volatile unsigned long *) 0x400243FC))  // PE2, PE1, PE0	

#define carLightsRed    0xE4
#define westGreen       0xCC
#define westYellow      0xD4
#define southGreen      0xE1
#define southYellow     0xE2
#define walkGreen       0x08
#define walkRed         0x02
#define walkOff         0x00

SType SFSM [9] =
//                              000,   001,     010,   011,  100,    101,   110,   111  
{
	{carLightsRed, walkRed,   100, {patt1, patt2, patt4, patt2, patt6, patt2, patt4, patt2}},
	{westGreen,    walkRed,   100, {patt2, patt2, patt3, patt3, patt3, patt3, patt3, patt3}},
	{westYellow,   walkRed,   100, {patt1, patt3, patt4, patt4, patt6, patt6, patt4, patt4}},
	{southGreen,   walkRed,   100, {patt4, patt4, patt5, patt5, patt5, patt5, patt5, patt5}},
	{southYellow,  walkRed,   100, {patt1, patt2, patt5, patt2, patt6, patt2, patt6, patt6}},
	{carLightsRed, walkGreen, 100, {patt1, patt7, patt7, patt7, patt6, patt7, patt7, patt7}},
	{carLightsRed, walkOff,   50, {patt1, patt9, patt8, patt9, patt6, patt9, patt8, patt8}},
	{carLightsRed, walkOff,   50, {patt4, patt4, patt4, patt4, patt4, patt4, patt4, patt9}},
	{carLightsRed, walkOff,   50, {patt2, patt2, patt2, patt2, patt2, patt2, patt2, patt1}},
	
};


// ***** 4. Global Declarations Section *****

// FUNCTION PROTOTYPES: Each subroutine defined
void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

// ***** 5. Subroutines Declaration Section *****
void Port_Init (void);
void PortLightsInit (void);
void PortWalkLightsInit (void);
void PortSensorsInit (void);

// void PLL_Init 				(void);
void SysTick_Init 	  (void);
void SysTick_Wait 	  (unsigned long delay);
void SysTick_Wait10ms (unsigned long delay);

void testLights      (void);
void testEastLights  (void);
void testSouthLights (void);
void testWalkLights  (void);


// ***** 6. MAIN Section *****
int main(void){ 
	
	// Declarations
	
	SType *Pt = 0x0; 			// State Pointer
  unsigned long 			   Input;
	volatile unsigned long delay;
	
  TExaS_Init(SW_PIN_PE210, LED_PIN_PB543210); // activate grader and set system clock to 80 MHz
	
	// PLL_Init ();
	SysTick_Init ();
	Port_Init ();
	
	// testLights();
	
	Pt = patt1;
	
  EnableInterrupts();
  while(1){
     CARLIGHTS  = Pt->OutCarLights;
		 WALKLIGHTS = Pt->OutPeatonLights;
		 SysTick_Wait10ms (Pt->Time);
		 Input     = SENSORS;
		 Pt        = Pt->Next[Input];
  }
}

// ***** 7. Subroutines Implementation Section *****

void Port_Init ()
{
	
	PortLightsInit 		 ();
	PortWalkLightsInit ();
	PortSensorsInit    ();
}

void PortLightsInit ()
{
	// LIGHTS in port B0 - B5
	volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000002;        // 1) B clock
  delay = SYSCTL_RCGC2_R;              // delay   
  GPIO_PORTB_AMSEL_R &= ~0x3F;         // 3) disable analog function
  GPIO_PORTB_PCTL_R  &= ~0x00FFFFFF;   // 4) GPIO clear bit PCTL  
  GPIO_PORTB_DIR_R   |=  0x3F;         // 5) PB5 -- PB0 output   
  GPIO_PORTB_AFSEL_R &= ~0x3F;         // 6) no alternate function
  GPIO_PORTB_PUR_R   &= ~0x03;         // disable pullup resistors on PB0 - PB1
  GPIO_PORTB_DEN_R    = 0x3F;          // 7) enable digital pins PB5-PB0        
	
}

void PortWalkLightsInit ()
{
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000020;        // 1) F clock
  delay = SYSCTL_RCGC2_R;              // delay   
  GPIO_PORTF_LOCK_R   = 0x4C4F434B;    // 2) unlock PortF PF0   -- NOT NEEDED --
  GPIO_PORTF_CR_R    |=  0x0A;         // allow changes to PF3, PF1  (Green, Red)   
  GPIO_PORTF_AMSEL_R &= ~0x0A;         // 3) disable analog function
  GPIO_PORTF_PCTL_R  &= ~0x0000F0F0;   // 4) GPIO clear bit PCTL  
  GPIO_PORTF_DIR_R   |=  0x0A;         // 5) PF3, PF1 output   
  GPIO_PORTF_AFSEL_R &= ~0x0A;      	 // 6) no alternate function
  // GPIO_PORTF_PUR_R = 0x11;             // enable pullup resistors on PF4,PF0   -- NOT NEEDED --
  GPIO_PORTF_DEN_R   |= 0x0A;          // 7) enable digital pins PF3,PF1        
}

void PortSensorsInit ()
{
	volatile unsigned long delay;				 // PE2 --> Walk Sensor, PE1 --> North Sensor, PE0 --> East Sensor.
	SYSCTL_RCGC2_R |= 0x00000010;        // 1) E clock
  delay = SYSCTL_RCGC2_R;              // delay   
  GPIO_PORTE_AMSEL_R &= ~0x07;         // 3) disable analog function
  GPIO_PORTE_PCTL_R  &= ~0x00000FFF;   // 4) GPIO clear bit PCTL  
  GPIO_PORTE_DIR_R   &= ~0x07;         // 5) PE2 -- PE0 input
  GPIO_PORTE_AFSEL_R &= ~0x07;      	 // 6) no alternate function
  GPIO_PORTE_DEN_R   |=  0x07;        // 7) enable digital pins PE2 -- PE0
}	


//void PLL_Init ()
//{
//  // 0) Use RCC2
//  SYSCTL_RCC2_R |=  0x80000000;  // USERCC2
//  // 1) bypass PLL while initializing
//  SYSCTL_RCC2_R |=  0x00000800;  // BYPASS2, PLL bypass
//  // 2) select the crystal value and oscillator source
//  SYSCTL_RCC_R = (SYSCTL_RCC_R &~0x000007C0)   // clear XTAL field, bits 10-6
//                 + 0x00000540;   // 10101, configure for 16 MHz crystal
//  SYSCTL_RCC2_R &= ~0x00000070;  // configure for main oscillator source
//  // 3) activate PLL by clearing PWRDN
//  SYSCTL_RCC2_R &= ~0x00002000;
//  // 4) set the desired system divider
//  SYSCTL_RCC2_R |= 0x40000000;   // use 400 MHz PLL
//  SYSCTL_RCC2_R = (SYSCTL_RCC2_R&~ 0x1FC00000)  // clear system clock divider
//                  + (4<<22);      // configure for 80 MHz clock
//  // 5) wait for the PLL to lock by polling PLLLRIS
//  while((SYSCTL_RIS_R&0x00000040)==0){};  // wait for PLLRIS bit
//  // 6) enable use of PLL by clearing BYPASS
//  SYSCTL_RCC2_R &= ~0x00000800;
//}

void SysTick_Init ()
{
	NVIC_ST_CTRL_R    = 0;                 // disable SysTick during setup
  NVIC_ST_RELOAD_R  = 0x00FFFFFF;        // maximum reload value
  NVIC_ST_CURRENT_R = 0;                 // any write to current clears it             
  NVIC_ST_CTRL_R    = 0x00000005;        // enable SysTick with core clock

}

// The delay parameter is in units of the 80 MHz core clock. (12.5 ns)
void SysTick_Wait(unsigned long delay){
  NVIC_ST_RELOAD_R = delay-1;  // number of counts to wait
  NVIC_ST_CURRENT_R = 0;       // any value written to CURRENT clears
  while((NVIC_ST_CTRL_R&0x00010000)==0){ // wait for count flag
  }
}
// 800000*12.5ns equals 10ms
void SysTick_Wait10ms(unsigned long delay){
  unsigned long i;
  for(i=0; i<delay; i++){
    SysTick_Wait(800000);  // wait 10ms
  }
}

void testLights ()
{
	testEastLights  ();
	testSouthLights ();
	testWalkLights  ();
}

void testEastLights ()
{
		GPIO_PORTB_DATA_R = 0x04;
		SysTick_Wait10ms (10);
		GPIO_PORTB_DATA_R = 0x02;
		SysTick_Wait10ms (10);
		GPIO_PORTB_DATA_R = 0x01;
		SysTick_Wait10ms (10);
}

void testSouthLights ()
{
		GPIO_PORTB_DATA_R = 0x20;
		SysTick_Wait10ms (10);
		GPIO_PORTB_DATA_R = 0x10;
		SysTick_Wait10ms (10);
		GPIO_PORTB_DATA_R = 0x08;
		SysTick_Wait10ms (10);
}

void testWalkLights ()
{
		unsigned int idx;
	  
	  GPIO_PORTF_DATA_R = 0x08;
		SysTick_Wait10ms (10);
		GPIO_PORTF_DATA_R = 0x02;
		SysTick_Wait10ms (10);
		
	  for (idx = 0; idx < 20; ++idx)
		{
			GPIO_PORTF_DATA_R = 0x00;
			SysTick_Wait10ms (5);
			GPIO_PORTF_DATA_R = 0x02;
			SysTick_Wait10ms (5);
		}

}
