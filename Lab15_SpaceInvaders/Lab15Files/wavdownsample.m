function wavdownsample( fullpathfilename, targetFs, targetFullPathFileName, varName, breakline)
%WAVDOWNSAMPLE Summary of this function goes here
%   Detailed explanation goes here
% Args:
% fullpathfilename, 
% targetFs, 
% targetFullPathFileName, 
% varName, 
% breakline

[Spls, fs] = audioread(fullpathfilename);
Spls = downsample(Spls, round(fs/targetFs));
Spls = round((Spls+ 1)* 7.5);
file = fopen(targetFullPathFileName, 'w');
fprintf(file, sprintf ('unsigned char %s [%d] = {', varName, length(Spls)));
for i=1 : breakline : length(Spls)
    if (i+breakline >= length(Spls))
        fprintf(file, '%d,', Spls(i:length(Spls)));
        fprintf(file, '};\n');
    else
        fprintf(file, '%d,', Spls(i:(i+breakline-1)));
        fprintf(file, '\n');
    end
end

end % wavdownsample

