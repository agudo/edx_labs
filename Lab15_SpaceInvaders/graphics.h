#ifndef __MYGRAPHICS__
#define __MYGRAPHICS__

void SpriteInit 			 (unsigned long lives); // Set initial scenario.
void SpriteEntryScreen (void);
void SpritesDraw       (void);  				  		// Move Sprites into screen Buffer.
void SpriteDrawFire    (void);
void SpriteDrawMissile (void);
void SpritesMoveShip   (unsigned long x); // Move Ship and / or update fires.

unsigned short SpritesEnemyMoves	(void); // Move All enemies sprites and returns weather or not our ship has been touched.

// Testing
// -------
void SpritesTesting (void);

#endif // __MYGRAPHICS__
