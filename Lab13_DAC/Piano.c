// Piano.c
// Runs on LM4F120 or TM4C123, 
// edX lab 13 
// There are four keys in the piano
// Daniel Valvano
// September 30, 2013

// Port E bits 3-0 have 4 piano keys

#include "Piano.h"
#include "..//tm4c123gh6pm.h"

// Const declarations
// ---------------------

#define KEY0_C  (* ((volatile unsigned long *) 0x40004010))  
#define KEY1_D  (* ((volatile unsigned long *) 0x40004020))  
#define KEY2_E  (* ((volatile unsigned long *) 0x40004040))  
#define KEY3_G  (* ((volatile unsigned long *) 0x40004080))  
	
// Forward declarations
// ---------------------
void delay(unsigned long msec);


// **************Piano_Init*********************
// Initialize piano key inputs
// Input: none
// Output: none
// Summary: Initialize Piano key inputs in port A
void Piano_Init(void)
{ 
  unsigned long volatile delay;
  SYSCTL_RCGC2_R |= 0x00000001; // activate port A
  delay = SYSCTL_RCGC2_R;
	
	// OUTPUT
  GPIO_PORTA_AMSEL_R &= ~0x0C;      // no analog
  GPIO_PORTA_PCTL_R &= ~0x00FFFF00; // regular function
	GPIO_PORTA_DIR_R &= 0xC3;         // make PA 2-5 in
  GPIO_PORTA_AFSEL_R &= ~0x3C;      // disable alt funct on PA 2-5
  GPIO_PORTA_DEN_R |= 0x3C;         // enable digital I/O on PA3, PA2
 
}
// **************Piano_In*********************
// Input from piano key inputs
// Input: none 
// Output: 0 to 15 depending on keys
// 0x01 is key 0 pressed, 0x02 is key 1 pressed,
// 0x04 is key 2 pressed, 0x08 is key 3 pressed
unsigned long Piano_In(void)
{
	unsigned long key_pressed = SILENCE;
	
	if (KEY0_C)   key_pressed = DO;
	if (KEY1_D)   key_pressed = RE;
	if (KEY2_E)   key_pressed = MI;
	if (KEY3_G)   key_pressed = SOL;
	
	return key_pressed;
}

// **************Piano_Testing *********************
// output  Test Piano
int Piano_Testing (void)
{
	while (1)
	{
		Piano_In ();
		delay (100);
	}
}

