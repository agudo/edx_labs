// ***** 0. Documentation Section *****
// SwitchLEDInterface.c for Lab 8
// Runs on LM4F120/TM4C123
// Use simple programming structures in C to toggle an LED
// while a button is pressed and turn the LED on when the
// button is released.  This lab requires external hardware
// to be wired to the LaunchPad using the prototyping board.
// January 11, 2014

// Lab 8
//      Jon Valvano and Ramesh Yerraballi
//      November 21, 2013

// ***** 1. Pre-processor Directives Section *****
#include "TExaS.h"
#include "tm4c123gh6pm.h"

// ***** 2. Global Declarations Section *****

// FUNCTION PROTOTYPES: Each subroutine defined
void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

// ***** 3. Subroutines Section *****

// PE0, PB0, or PA2 connected to positive logic momentary switch using 10 k ohm pull down resistor
// PE1, PB1, or PA3 connected to positive logic LED through 470 ohm current limiting resistor
// To avoid damaging your hardware, ensure that your circuits match the schematic
// shown in Lab8_artist.sch (PCB Artist schematic file) or 
// Lab8_artist.pdf (compatible with many various readers like Adobe Acrobat).

// ***** 4. Jagudo's stuff *****

#define PE0   (*((volatile unsigned long *)0x40024004))
#define PE1   (*((volatile unsigned long *)0x40024008))
	
unsigned long in,out;

// TOOLS
// ------
void delay        (unsigned long time);

// SWITCH
// ------
void 							Switch_Init (void);
unsigned long int Switch_Read (void); 

// LEDS
// ----
void 						  LED_Init   (void);
unsigned long int LED_Read   (void);
void 						  LED_On     (void) {GPIO_PORTE_DATA_R |= 0x02;};
void 							LED_Off    (void) {GPIO_PORTE_DATA_R &= 0xFFFD;};
void              LED_Change (void);



int main(void){ 
//**********************************************************************
// The following version tests input on PE0 and output on PE1
//**********************************************************************
  TExaS_Init(SW_PIN_PE0, LED_PIN_PE1);  // activate grader and set system clock to 80 MHz
  
	
  EnableInterrupts();           // enable interrupts for the grader
  
	Switch_Init();
	LED_Init   ();
	LED_On     ();
	  
	while(1)
	{
			unsigned long change = Switch_Read ();
			if  (change) LED_Change();
			else				 LED_On    ();
			delay (10);
  }
  
}


// Subroutine to delay in units of half seconds
// Inputs: Number of half seconds to delay
// Outputs: None
// simple delay function
// which delays time*500 milliseconds
// assuming 80 MHz clock
// The following C function can be used to delay. 
// The number 133333 assumes 6 cycles per loop (10ms/12.5ns/6). 
// The Keil optimization is set at Level 0 (-O0) and the �Optimize for Time� mode is unchecked.

void delay(unsigned long time)
{
  unsigned long i;
  while(time > 0){
    i = 26666; //26666 for 16MHz; // 80MHz clock: 133333 for 16Mhz
    while(i > 0){
      i = i - 1;
    }
    time = time - 1;
  }
}

void Switch_Init (void)
{
	unsigned long volatile initDelay;
	
  // Port A, B, E clock
	SYSCTL_RCGC2_R     |= 0x13;           
  initDelay           = SYSCTL_RCGC2_R;       // wait 3-5 bus cycles
	
	// Port E
  GPIO_PORTE_DIR_R   &= ~0x01;        // PE0 input 
  GPIO_PORTE_AFSEL_R &= ~0x01;      // not alternative
  GPIO_PORTE_AMSEL_R &= ~0x01;      // no analog
  GPIO_PORTE_PCTL_R  &= ~0x0000000F; // bits for PE0
  GPIO_PORTE_DEN_R   |= 0x01;         // enable PE0
	GPIO_PORTE_PUR_R   &= ~0x01;        // Pull up disable, we're going to connect it outside
	
	// Port B
  GPIO_PORTB_DIR_R   &= ~0x01;        // PB0 input 
  GPIO_PORTB_AFSEL_R &= ~0x01;      // not alternative
  GPIO_PORTB_AMSEL_R &= ~0x01;      // no analog
  GPIO_PORTB_PCTL_R  &= ~0x0000000F; // bits for PB0
  GPIO_PORTB_DEN_R   |= 0x01;         // enable PB0
	GPIO_PORTB_PUR_R   &= ~0x01;        // Pull up disable, we're going to connect it outside
	
	// Port A
  GPIO_PORTA_DIR_R   &= ~0x04;        // PA2 input 
  GPIO_PORTA_AFSEL_R &= ~0x04;      // not alternative
  GPIO_PORTA_AMSEL_R &= ~0x04;      // no analog
  GPIO_PORTA_PCTL_R  &= ~0x00000F00; // bits for PA2
  GPIO_PORTA_DEN_R   |= 0x04;         // enable PA2
	GPIO_PORTA_PUR_R   &= ~0x04;        // Pull up disable, we're going to connect it outside	
}

void LED_Init (void)
{
	unsigned long volatile initDelay;
	
	  // Port A, B, E clock
	SYSCTL_RCGC2_R     |= 0x13;           
  initDelay           = SYSCTL_RCGC2_R;       // wait 3-5 bus cycles

	// Port D
  GPIO_PORTE_DIR_R 	 |= 0x02;           // PE1 output
  GPIO_PORTE_AFSEL_R &= ~0x02;          // not alternative
  GPIO_PORTE_AMSEL_R &= ~0x02;          // no analog
  GPIO_PORTE_PCTL_R  &= ~0x000000F0;    // bits for PD1
  GPIO_PORTE_DEN_R   |= 0x02;           // enable PD1
	GPIO_PORTE_PUR_R   &= ~0x02;          // Pull up disable, we're going to connect it outside
	
	// Port B
  GPIO_PORTB_DIR_R 	 |= 0x02;           // PB1 output
  GPIO_PORTB_AFSEL_R &= ~0x02;          // not alternative
  GPIO_PORTB_AMSEL_R &= ~0x02;          // no analog
  GPIO_PORTB_PCTL_R  &= ~0x000000F0;    // bits for PB1
  GPIO_PORTB_DEN_R   |= 0x02;           // enable PB1
	GPIO_PORTB_PUR_R   &= ~0x02;          // Pull up disable, we're going to connect it outside
	
	// Port A
  GPIO_PORTA_DIR_R 	 |= 0x04;           // PA3 output
  GPIO_PORTA_AFSEL_R &= ~0x04;          // not alternative
  GPIO_PORTA_AMSEL_R &= ~0x04;          // no analog
  GPIO_PORTA_PCTL_R  &= ~0x00000F00;    // bits for PA3
  GPIO_PORTA_DEN_R   |= 0x04;           // enable PA3
	GPIO_PORTA_PUR_R   &= ~0x04;          // Pull up disable, we're going to connect it outside
}


unsigned long int Switch_Read (void) 
{
	in = PE0; 
  return PE0;

};

void LED_Change (void) 
{	
	out = (LED_Read()^0x02); 
	GPIO_PORTE_DATA_R  = out;
}

unsigned long int LED_Read (void)
{
	in = PE1;
	return PE1;
};
