// Sound.h
// Runs on TM4C123 or LM4F120
// Prototypes for basic functions to play sounds from the
// original Space Invaders.
// Jonathan Valvano
// November 19, 2012

enum FX_Sound{ fx_none,
	             fx_silence_wave,
							 fx_sin_wave,
							 fx_shoot,
							 fx_invaderkilled,
							 fx_explosion,
							 fx_fastinvader1,
							 fx_fastinvader2,
							 fx_fastinvader3,
							 fx_fastinvader4,
						   fx_highpitch
						 };


// General utilities
void Sound_Init(void);
void Sound_Play(const unsigned char *pt, unsigned long count, enum FX_Sound fx_sound_index);

// Timer Tools
void Play(void);
						 
// Game Tools
void Sounds_Play (void);

// Predefined sounds
void Sound_Shoot(void);
void Sound_Killed(void);
void Sound_Explosion(void);
void Sound_Fastinvader1(void);
void Sound_Fastinvader2(void);
void Sound_Fastinvader3(void);
void Sound_Fastinvader4(void);
void Sound_Highpitch(void);

// Testing Tools
void Sound_Test (void);
