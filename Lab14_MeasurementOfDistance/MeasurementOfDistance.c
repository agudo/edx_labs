// MeasurementOfDistance.c
// Runs on LM4F120/TM4C123
// Use SysTick interrupts to periodically initiate a software-
// triggered ADC conversion, convert the sample to a fixed-
// point decimal distance, and store the result in a mailbox.
// The foreground thread takes the result from the mailbox,
// converts the result to a string, and prints it to the
// Nokia5110 LCD.  The display is optional.
// April 8, 2014

/* This example accompanies the book
   "Embedded Systems: Introduction to ARM Cortex M Microcontrollers",
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2013

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

// Slide pot pin 3 connected to +3.3V
// Slide pot pin 2 connected to PE1 and PD3
// Slide pot pin 1 connected to ground


#include "ADC.h"
#include "UART.h"
#include "..//tm4c123gh6pm.h"
#include "Nokia5110.h"
#include "TExaS.h"

void EnableInterrupts(void);  // Enable interrupts

extern unsigned char String[10]; 
unsigned long Distance; // units 0.001 cm
unsigned long ADCdata;  // 12-bit 0 to 4095 sample
unsigned long Flag;     // 1 means valid Distance, 0 means Distance is empty

//********Convert****************
// Convert a 12-bit binary ADC sample into a 32-bit unsigned
// fixed-point distance (resolution 0.001 cm).  Calibration
// data is gathered using known distances and reading the
// ADC value measured on PE1.  
// Overflow and dropout should be considered 
// Input: sample  12-bit ADC sample
// Output: 32-bit distance (resolution 0.001cm)

// CALIBRATION DATA (Using XL regressor)
// Double const regressor coefficients
const double m = 1.373413946; // 1.46484375; // Real board for 6.0 cm  // 0.48828125; Simulation for 2cm
const double b = 168.0977665; // 4.54747E-13; // Real board for 6.0 cm  // 2.27374E-13; Simulation for 2cm

unsigned long Convert(unsigned long sample){
  unsigned long converted_value = m * sample + b;
	return converted_value;
}

// Initialize SysTick interrupts to trigger at 40 Hz, 25 ms
void SysTick_Init(unsigned long period){
	NVIC_ST_CTRL_R = 0;               // disable SysTick during setup
  NVIC_ST_RELOAD_R = period-1;         // reload value for 1.13ms (assuming 80MHz) 90908, 18180 (assuming 16MHz)
  NVIC_ST_CURRENT_R = 0;            // any write to current clears it
  NVIC_SYS_PRI3_R = NVIC_SYS_PRI3_R&0x00FFFFFF; // priority 0               
  NVIC_ST_CTRL_R = 0x00000007;  		// enable with core clock and interrupts
}
// executes every 25 ms, collects a sample, converts and stores in mailbox
void SysTick_Handler(void)
{ 
	  ADCdata = ADC0_In();
    Distance = Convert(ADCdata+1);
		Flag = 1;
}

//-----------------------UART_ConvertDistance-----------------------
// Converts a 32-bit distance into an ASCII string
// Input: 32-bit number to be converted (resolution 0.001cm)
// Output: store the conversion in global variable String[10]
// Fixed format 1 digit, point, 3 digits, space, units, null termination
// Examples
//    4 to "0.004 cm"  
//   31 to "0.031 cm" 
//  102 to "0.102 cm" 
// 2210 to "2.210 cm"
//10000 to "*.*** cm"  any value larger than 9999 converted to "*.*** cm"
// void UART_ConvertDistance(unsigned long n); // Lab 11. It is in include UART.h

int main1			(void); // Test ADC
int main2			(void);
int main_main (void);

int main (void)
{
	// main1();
	// main2();
	main_main();
	
}

int main_main(void)
{ 
  volatile unsigned long delay;
	Flag = 0;
	
	TExaS_Init(ADC0_AIN1_PIN_PE2, SSI0_Real_Nokia5110_Scope);
  
	// initialize ADC0, channel 1, sequencer 3
	ADC0_Init();    
	
	// initialize Nokia5110 LCD (optional)
	Nokia5110_Init();             
	
	// initialize SysTick for 40 Hz interrupts, 25 ms
	SysTick_Init (2000000); // 2e6 = 80MHz / 40 Hz = 80MHz * 25E-3
	
	// initialize profiling on PF1 (optional)
	
	
	// wait for clock to stabilize
  EnableInterrupts();

	// print a welcome message  (optional)
	Nokia5110_Clear     ();
	Nokia5110_SetCursor (0, 0);
	Nokia5110_OutString ((unsigned char*) ("Jagudo"));
	Nokia5110_SetCursor (0, 1);
	Nokia5110_OutString ((unsigned char*) ("Lab 14"));
	Nokia5110_SetCursor (0, 2);
  Nokia5110_OutString ((unsigned char*) ("Edx.org"));
	
  while(1){ 
		// read mailbox
    if (Flag)
		{
			Nokia5110_SetCursor (0, 3);
			UART_ConvertDistance(Distance); // from Lab 11. Set Global Variable String
			Nokia5110_OutString (String);		
			Flag = 0;
		}
  }
}

int main1(void){ 
  TExaS_Init(ADC0_AIN1_PIN_PE2, SSI0_Real_Nokia5110_Scope);
  ADC0_Init();    // initialize ADC0, channel 1, sequencer 3
  EnableInterrupts();
  while(1){ 
    ADCdata = ADC0_In();
  }
}
int main2(void){ 
  TExaS_Init(ADC0_AIN1_PIN_PE2, SSI0_Real_Nokia5110_NoScope);
  ADC0_Init();    // initialize ADC0, channel 1, sequencer 3
  Nokia5110_Init();             // initialize Nokia5110 LCD
  EnableInterrupts();
  while(1){ 
    ADCdata = ADC0_In();
    Nokia5110_SetCursor(0, 0);
    Distance = Convert(ADCdata);
    UART_ConvertDistance(Distance); // from Lab 11
    Nokia5110_OutString(String);    
  }
}

